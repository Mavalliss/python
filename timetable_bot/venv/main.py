import vk_api
from vk_api.bot_longpoll import VkBotLongPoll
from vk_api.bot_longpoll import VkBotEventType
import json
import tables as tab

def showw(day):
    for i in tab.show(day):
        res += tab.time.__getitem__(j) + i + "\n"
        j += 1

vk = vk_api.VkApi(token="eb002b30ae94b1a4f6dc66a48d9e1ce468bac4a396c226ddb56d7c7eea719985b27d0dc1ee0dd204be009")
vk._auth_token()
vk.get_api()

keyboard = {
    "one_time": False,
    "buttons": [
        [{
            "action": {
                "type": "text",
                "payload": "{\"button\": \"1\"}",
                "label": "Понедельник"
            },
            "color": "positive"
        },
            {
                "action": {
                    "type": "text",
                    "payload": "{\"button\": \"1\"}",
                    "label": "Вторник"
                },
                "color": "positive"
        },
            {
                "action": {
                    "type": "text",
                    "payload": "{\"button\": \"1\"}",
                    "label": "Среда"
                },
                "color": "positive"
        }],
        [
            {
                "action": {
                    "type": "text",
                    "payload": "{\"button\": \"1\"}",
                    "label": "Четверг"
                },
                "color": "positive"
        },
            {
                "action": {
                    "type": "text",
                    "payload": "{\"button\": \"1\"}",
                    "label": "Пятница"
                },
                "color": "positive"
        },
            {
                "action": {
                    "type": "text",
                    "payload": "{\"button\": \"1\"}",
                    "label": "Скрыть"
                },
                "color": "secondary"
            }
        ]
    ]
}
keyboard_empty = {
    "one_time": True,
    "buttons": [

    ]
}

keyboard = json.dumps(keyboard, ensure_ascii=False).encode('utf-8')
keyboard = str(keyboard.decode('utf-8'))
keyboard_empty = json.dumps(keyboard_empty, ensure_ascii=False).encode('utf-8')
keyboard_empty = str(keyboard_empty.decode('utf-8'))

longpoll = VkBotLongPoll(vk, 187702392)

while True:
    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            if "UPDATE" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": "Клавиатура", "random_id": 0,
                                            "keyboard": keyboard})
            elif "Скрыть" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": "ok", "random_id": 0,
                                            "keyboard": keyboard_empty})
            elif "Понедельник" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": tab.show("Понедельник"), "random_id": 0,
                                            "keyboard": keyboard_empty})
            elif "Вторник" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": tab.show("Вторник"), "random_id": 0,
                                            "keyboard": keyboard_empty})
            elif "Среда" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": tab.show("Среда"), "random_id": 0,
                                            "keyboard": keyboard_empty})
            elif "Четверг" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": tab.show("Четверг"), "random_id": 0,
                                            "keyboard": keyboard_empty})
            elif "Пятница" in event.object.text:
                vk.method("messages.send", {"peer_id": event.object.peer_id, "message": tab.show("Пятница"), "random_id": 0,
                                            "keyboard": keyboard_empty})